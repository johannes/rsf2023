global hallo
def hallo():
    print("Hallo Nutzer!")


global counter
def counter():
    # Lese den aktuellen Wert aus der Datei
    f = open("besucher.txt", "r")
    currentValue = f.read()
    print(currentValue)
    newValue = int(currentValue) + 1
    # Schreibe den aktualisierten Wert zurück in die Datei
    f.close()
    f = open("besucher.txt", "w")
    f.write(str(newValue))
    f.close()







def createTable(arrFC, angebotFC, h1):
    tabelleFC = html.H1(h1)
    table = html.TABLE(Class="angebottabelle")
    row = html.TR()
    row <= html.TH("Speisen")
    row <= html.TH("Getränke")
    row <= html.TH("Programm")
    table <= row
    row = (html.TR(html.TD(x) for x in row) for row in arrFC)
    table <= row
    tabelleFC <= table
    document[angebotFC] <= tabelleFC
    return tabelleFC

# Anzahl der Besucher
from browser import document
besucherAnzahl = 0

# Funktion, die jedes Mal aufgerufen wird, wenn ein Besucher die Seite besucht
def besucherZaehlen():
    global besucherAnzahl
    besucherAnzahl += 1

    # schreibe die Anzahl der Besucher in eine Datei
    with open('besucherzahl.txt', 'w') as file:
        file.write(str(besucherAnzahl))

    # aktualisiere das div-Element
    document['besucherzaehler'].innerHTML = "Anzahl der Besucher: " + str(besucherAnzahl)
    print(besucherAnzahl)
    #besucherZaehlen()

