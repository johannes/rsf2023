

arrSa = [["16:30", "Vorstellung der Gruppen auf dem Festplatz beim Sportplatz, Getränkeverkauf am Jugendhaus"],
         ["17:30", "Offizieller Festbeginn mit Fassanstioch durch Frau OB Petzold-Schick auf dem Festplatz"],
         ["18:00", "Festumzug vom Festplatz über den Marktplatz drch das Stadttor"],
         ["19:00", "Fanfarenzüge aus Heidelsheim und Volterra ziehen durch das Fest"],
         ["19:00", "Mittelalterliche Tänze bei der Stadtkapelle"],
         ["19:30", "Trommler Musici delle Contrade bei den Melkkiwwlreider"],
         ["20:00", "Modenschau mit mittelalterlichen Gewändern bei den Melkkiwwlreidern"],
         ["21:00", "Nachtwächter und Türmer singen beim Stadttor"],
         ["21:00", "DJ Hansi in der FC Bar"],
         ["21:30", "Mittelalterliche Tänze bei der Stadtkapelle"]]

arrSo = [["10:30", "Ökumenischer Festgottesdienst auf dem Kirchturmplatz, anschließend Festbeginn"],
         ["11:30", "Tafeley bei den Melkkiwwlreidern (ausverkauft)"],
         ["15:00", "Auftritt der Tanzgruppe der Bürgerwehr beim Schafsbrunnen bei der Bürgerwehr"],
         ["16:00", "Ridiculus Artifex - Jonglage beim Lager Communio Agricolae in der Merianstraße"],
         ["17:00", "Trommler Musici delle Contrade beim Stadttor"],
         ["17:00", "Drehorgelspieler-Duo aus Durlach zeiht durch das Fest"],
         ["17:00", "Bauchtanzgruppe Chorus Pyramis bei der Bürgerwehr"],
         ["17:30", "Fahnenschwinger aus Heidelsheim und Volterra ziehen begleitet vom Fanfarenzug Heidelsheim durch das Fest"],
         ["18:00", "Quiltverlosung im Kirchgarten"],
         ["18:00", "Mittelalterliche Tänze bei der Stadtkapelle"],
         ["18:30", "1. Fanfarenzug Bruchsal zieht durch die Gassen"],
         ["18:30", "Trommlergruppe Tamburini Carini bei den Melkkiwwlreidern"],
         ["19:30", "Modenschau mit mittelalterlichen Gewändern bei den Melkkiwwlreidern"],
         ["20:00", "Mittelalterliche Tänze bei der Stadtkapelle"],
         ["21:30", "Säbel- und Lichtertanz mit der Gruppe Chorus Pyramis bei der Bürgerwehr"]]

arrMo = [["11:00", "Festbeginn mit Mittagstisch"],
	 ["", "Am Vormittag besuchen heidelsheimer Kindergärten das Fest"],
         ["18:00", "Tanzgruppen der Bürgerwehr und der Melkkiwwlreider bei den Melkkiwwlreidern"],
         ["18:30", "Tanzgruppen der Bürgerwehr und der Melkkiwwlreider beim Schafsbrunnen bei der Bürgerwehr"],
         ["18:00", "LIVEMUSIK: Lönneberga mit Vorband New Vintage bei den Melkkiwwlreidern"],
         ["19:00", "Fanfarenzüge der freiwilligen Feuerwehr Weiher zieht durch das Fest"],
         ["19:50", "LIVEMUSIK: Ahjoo! beim OWK"],
         ["20:30", "LIVEMUSIK: Sunday Rest beim OWK"],
         ["20:30", "LIVEMUSIK: Uptown Band beim Turnverein und der Bürgerwehr"]]


arrProgramm = [["16:30", "17:00", "21:00"],
               ["Festeröffnung", "Tanz", "Gesang"],
               ["16:30", "17:00", "21:00"],
               ["Festeröffnung", "Tanz", "Gesang"],
               ["16:30", "17:00", "21:00"],
               ["Festeröffnung", "Tanz", "Gesang"]]

# Angebote der Vereine:


arrFC = [["Biere der Brauerei Hoepfner", "Weine der Weingüter Gravino und Kern", "große Auswahl an alkoholfreien Getränken"],
            ["Kässpatzn mit Bergkäse aus der Käserei Gunzesried", "Mediterranes Tomaten-Mozzarella-Sandwich ", "Fleischküchle mit Kartoffelsalat", "Grillwurst, Currywurst, Pommes", "FC-07 Gulaschsuppe"],
            ["FC-Bar"]]

arrMKR = [["Bauhöfer No 5", "Sekt- und Weinbar vom Weingut Georg Benz", "Liköre und Schnäpse aus eigener Herstellung", "Mokka- und Teezelt"],
         ["Spanferkelbraten mit Biersoße und Kraut im Fladen", "Stockbrot", "Grumbiera-Stengel", "Süße Leckereien im Mokkazelt"],
         ["Sa 19:30: Trommler Musici delle Contrade", "Sa 20 Uhr: Modenschau", "So 11:30 Uhr: Tafeley", "So 19:30 Uhr: Modenschau", "Mo 18 Uhr: Tanzgruppen", "Mo 18 Uhr: New Vintage und Lönneberga"]]

arrTV = [["Bierstand mit Bargetränken", "Kaffeebar"],
            ["Heydolfesheimer vegetarisch", "Maultaschenburger", "Maultaschen mit Kartoffelsalat", "Bratwürste rot/weiß", "Currywurst", "Pommes"],
            ["Bierstand mit Bargetränken", "Mo 20:30 Uhr: Uptown Band"]]

arrSB = [["Wiernsheimer Adlerbräu (hell)", "ab 20 Uhr: Bargetränke"],
            ["Sängerspießbraten vom offenen Feuer", "Forelle gebacken (aus heimischer Zucht)", "Maultaschen in Brühe", "Pommes", "süße Waffeln"],
            ["Bierstand mit Bargetränken"]]

arrOWK = [["Andechser Hefeweizen", "Göcklinger Helles", "Aperol Spezial", "Kalte Ente", "Gin Tonic"],
            ["Grillschinken", "Nongs Gemüserolle"],
            ["Mo 19:50 Uhr: Livemusik mit Ahjoo!", "Mo 20:30 Uhr: Livemusik mit Sunday Rest"]]

arrV = [["ital. Rotwein", "Chianti", "Espresso"],
            ["ital. Spezialitäten"],
            ["Mitelalterliche Gruppen ziehen durch das Fest"]]

arrBW = [["Wiernsheimer Adlerbräu (Schwarzbier)", "Beerentrank für lustige Weiber, mit und ohen Schädelweh"],
            ["Bürgersteak", "deftige Hausmacher", "Obazter und Bibbeleskäs auf frischem Bauernbrot", "Rahmfleck"],
            ["So 15 Uhr: Auftritt Tanzgruppe", "So 17 Uhr: Bauchtanz mit Chorus Pyramis", "Säbel- und Lichtertanz mit Chorus Pyramis", "Mo 18:30 Uhr: Tanzgruppen",  "Mo 20:30 Uhr Uptown Band"]]

arrSK = [["Caipi Bar", "Sektbar"],
            ["brasil. Churrasco", "Wurstsalat", "Heiße Wurst", "Pommes", "Apfelküchle"],
            ["So 12 Uhr: Mittagstisch: Gegrilltes Lachsfilet mit Nudeln", "Sa 19:00 Uhr: Mittelalterliche Tänze", "Sa 21:30 Uhr: Mittelalterliche Tänze", "So 18:00 Uhr: Mittelalterliche Tänze", "So 20:00 Uhr: Mittelalterliche Tänze"]]

arrTC = [["Caipi Bar", "Sektbar"],
            ["brasil. Churrasco", "Wurstsalat", "Heiße Wurst", "Pommes", "Apfelküchle"],
            ["So 12 Uhr: Mittagstisch: Gegrilltes Lachsfilet mit Nudeln"]]

arrFCB = [["Cocktails", "Longdrinks", "Weinbar", "Shots"],
            ["Deftige Handbrotzeit"],
            ["Chill-Lounge", "Sa 21 Uhr: Disco mit DJ Hansi", "Mo Abend: Musik mit DJ Hansi"]]

arrKKS = [["keine"],
            ["Wildbratwürst to go", "Crepes"],
            ["Schießstand"]]            
            
            
arrSQ = [["keine"],
            ["keine"],
            ["Quiltverlosung"]]           

# Namen der Vereine und deren Abkürzungen:

arrAbk = [["MKR", "Melkkiwwlreider"],
           ["FC", "FC 07 Heidelsheim"],
           ["TV", "Turnverein Heidelsheim"],
           ["SB", "Sängerbund Liederkranz"],
           ["OWK", "Odenwaldklub"],
           ["V", "Freundeskreis Volterra"],
           ["BW", "Bürgerwehr Heydolfesheim"],
           ["SK", "Stadtkapelle"],
           ["TC", "TC 79"],
           ["FCB", "FC Bar"],
           ["KKS", "KK-Schützenverein"],
           ["SQ", "Saalbach-Quilter"]]







