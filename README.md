ich möchte den folgenden Code in eine Funktion auslagern, der ich nur das Array arrFC, die id angebotFC und den Inhalt von H1 übergeben möchte wie realisiere ich das in Brython?
Hier der Code:
            tabelleFC = html.H1("FC 07 Heidelsheim")
            table = html.TABLE(Class = "angebottabelle")
            row = html.TR()
            row <= html.TH("Speisen")
            row <= html.TH("Getränke")
            row <= html.TH("Programm")
            table <= row
            row = (html.TR(html.TD(x) for x in row) for row in angebote.arrFC)
            table <= row
            tabelleFC <= table
            document["angebotFC"] <= tabelleFC

Um diesen Code in eine Funktion auszulagern, in der Sie nur das Array arrFC, die ID angebotFC und den Inhalt von H1 übergeben, können Sie folgenden Code verwenden:

def createTable(arrFC, angebotFC, h1):
    tabelleFC = html.H1(h1)
    table = html.TABLE(Class="angebottabelle")
    row = html.TR()
    row <= html.TH("Speisen")
    row <= html.TH("Getränke")
    row <= html.TH("Programm")
    table <= row
    row = (html.TR(html.TD(x) for x in row) for row in arrFC)
    table <= row
    tabelleFC <= table
    document[angebotFC] <= tabelleFC
    return tabelleFC

# Aufruf der Funktion
createTable(angebote.arrFC, 'angebotFC', 'FC 07 Heidelsheim')
